module.exports = {
  // ...other vue-cli plugin options...
  pwa: {
    name: "Party Up!",
    themeColor: "#453191",
    msTileColor: "#d8b9fa",
    appleMobileWebAppCapable: "yes",
    appleMobileWebAppStatusBarStyle: "black",
    manifestOptions: {
      background_color: "#d8b9fa"
    }
  }
};
