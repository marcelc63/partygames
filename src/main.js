import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import "./main.css";
import "./registerServiceWorker";

Vue.config.productionTip = false;

//Socket
import VueSocketIO from "vue-socket.io";
Vue.use(
  new VueSocketIO({
    debug: true,
    connection: process.env.VUE_APP_API
  })
);

//Modal
import SweetModal from "sweet-modal-vue/src/plugin.js";
Vue.use(SweetModal);

//Chat
import VueChatScroll from "vue-chat-scroll";
Vue.use(VueChatScroll);

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
