let base = { path: "/spyfall", name: "Spyfall" };

export default [
  {
    path: base.path,
    name: `${base.name}`,
    component: () => import("../views/spyfall/Mode.vue")
  },
  {
    path: `${base.path}/rules`,
    name: `${base.name} Rules`,
    component: () => import("../views/spyfall/Rules.vue")
  },
  {
    path: `${base.path}/reveal`,
    name: `${base.name} Reveal`,
    component: () => import("../views/spyfall/Reveal.vue")
  },
  {
    path: `${base.path}/mission`,
    name: `${base.name} Mission`,
    component: () => import("../views/spyfall/Mission.vue")
  }
];
