import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "home",
    component: () => import("../v2/Home.vue")
  },
  {
    path: "/setup",
    name: "setup",
    component: () => import("../v2/Setup.vue")
  },
  {
    path: "/wrapper",
    name: "wrapper",
    component: () => import("../v2/Wrapper.vue")
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
