let base = { path: "/avalon", name: "Avalon" };

export default [
  {
    path: base.path,
    name: `${base.name}`,
    component: () => import("../views/avalon/Mode.vue")
  },
  {
    path: `${base.path}/rules`,
    name: `${base.name} Rules`,
    component: () => import("../views/avalon/Rules.vue")
  },
  {
    path: `${base.path}/reveal`,
    name: `${base.name} Reveal`,
    component: () => import("../views/avalon/Reveal.vue")
  },
  {
    path: `${base.path}/mission`,
    name: `${base.name} Mission`,
    component: () => import("../views/avalon/Mission.vue")
  }
];
