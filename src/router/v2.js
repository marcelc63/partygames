let base = "/players";

export default [
  {
    path: base,
    name: "avalon",
    component: () => import("../views/Players.vue")
  }
];
