import uniqid from "uniqid";
import _ from "lodash";

let test = [
  {
    name: "a",
    id: "a",
    type: "gamemaster"
  },
  {
    name: "b",
    id: "b",
    type: "player"
  },
  {
    name: "c",
    id: "c",
    type: "player"
  },
  {
    name: "d",
    id: "d",
    type: "player"
  },
  {
    name: "e",
    id: "e",
    type: "player"
  }
];

export default {
  state: {
    players: [...test],
    name: "",
    room: "",
    profile: "",
    id: ""
  },
  mutations: {
    players: function(state, pkg) {
      let { type, payload } = pkg;
      if (type === "ADD") {
        state.players.push({
          name: payload.name,
          id: uniqid(),
          type: "offline"
        });
      } else if (type === "REMOVE") {
        state.players = state.players.filter(x => x.id != payload.id);
      } else if (type === "GAMEMASTER") {
        state.players.push({
          name: payload.name,
          id: payload.id,
          type: payload.type
        });
        state.profile = "gamemaster";
        state.id = payload.id;
      } else if (type === "ONLINE") {
        state.players.push({
          name: payload.name,
          id: payload.id,
          type: payload.type
        });
        state.profile = "online";
        state.id = payload.id;
      } else if (type === "UPDATE") {
        state.players = payload.players;
      }
    },
    name: function(state, pkg) {
      let { type, payload } = pkg;
      if (type === "SET") {
        state.name = payload.name;
      }
    },
    room: function(state, pkg) {
      let { type, payload } = pkg;
      if (type === "SET") {
        state.room = payload.room;
      }
    },
    exit: function(state, pkg) {
      let { type, payload } = pkg;
      if (type === "EXIT") {
        state.players = [];
        state.room = "";
        state.name = "";
        state.profile = "";
      }
    }
  },
  actions: {},
  getters: {
    players: function(state) {
      return state.players;
    },
    name: function(state) {
      return state.name;
    },
    room: function(state) {
      return state.room;
    },
    profile: function(state) {
      return state.profile;
    },
    id: function(state) {
      return state.id;
    }
  }
};
