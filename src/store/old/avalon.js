import uniqid from "uniqid";
import _ from "lodash";

let rules = {
  players: [
    { players: 5, resistance: 3, spies: 2 },
    { players: 6, resistance: 4, spies: 2 },
    { players: 7, resistance: 4, spies: 3 },
    { players: 8, resistance: 5, spies: 3 },
    { players: 9, resistance: 6, spies: 3 },
    { players: 10, resistance: 6, spies: 4 },
    { players: 11, resistance: 7, spies: 4 },
    { players: 12, resistance: 8, spies: 4 }
  ],
  missions: [
    {
      players: 5,
      missions: [
        { players: 2, fails: 1 },
        { players: 3, fails: 1 },
        { players: 2, fails: 1 },
        { players: 3, fails: 1 },
        { players: 3, fails: 1 }
      ]
    },
    {
      players: 6,
      missions: [
        { players: 2, fails: 1 },
        { players: 3, fails: 1 },
        { players: 4, fails: 1 },
        { players: 3, fails: 1 },
        { players: 4, fails: 1 }
      ]
    },
    {
      players: 7,
      missions: [
        { players: 2, fails: 1 },
        { players: 3, fails: 1 },
        { players: 3, fails: 1 },
        { players: 4, fails: 2 },
        { players: 4, fails: 1 }
      ]
    },
    {
      players: 8,
      missions: [
        { players: 3, fails: 1 },
        { players: 4, fails: 1 },
        { players: 4, fails: 1 },
        { players: 5, fails: 2 },
        { players: 5, fails: 1 }
      ]
    },
    {
      players: 9,
      missions: [
        { players: 3, fails: 1 },
        { players: 4, fails: 1 },
        { players: 4, fails: 1 },
        { players: 5, fails: 2 },
        { players: 5, fails: 1 }
      ]
    },
    {
      players: 10,
      missions: [
        { players: 3, fails: 1 },
        { players: 4, fails: 1 },
        { players: 4, fails: 1 },
        { players: 5, fails: 2 },
        { players: 5, fails: 1 }
      ]
    },
    {
      players: 11,
      missions: [
        { players: 3, fails: 1 },
        { players: 4, fails: 1 },
        { players: 4, fails: 1 },
        { players: 5, fails: 2 },
        { players: 5, fails: 1 }
      ]
    },
    {
      players: 12,
      missions: [
        { players: 3, fails: 1 },
        { players: 4, fails: 1 },
        { players: 4, fails: 1 },
        { players: 5, fails: 2 },
        { players: 5, fails: 1 }
      ]
    }
  ]
};

let test = {
  players: [
    { name: "a", role: "merlin", team: "resistance", id: "a" },
    { name: "b", role: "member", team: "resistance", id: "b" },
    { name: "c", role: "member", team: "resistance", id: "c" },
    { name: "d", role: "member", team: "spies", id: "d" },
    { name: "e", role: "member", team: "spies", id: "e" }
  ],
  missions: [
    {
      players: 2,
      fails: 1,
      status: "pending",
      votes: {
        succeed: 0,
        fail: 0
      }
    },
    {
      players: 3,
      fails: 2,
      status: "pending",
      votes: {
        succeed: 0,
        fail: 0
      }
    },
    {
      players: 2,
      fails: 1,
      status: "pending",
      votes: {
        succeed: 0,
        fail: 0
      }
    },
    {
      players: 3,
      fails: 1,
      status: "pending",
      votes: {
        succeed: 0,
        fail: 0
      }
    },
    {
      players: 3,
      fails: 1,
      status: "pending",
      votes: {
        succeed: 0,
        fail: 0
      }
    }
  ]
};

export default {
  state: {
    players: [],
    missions: [],
    track: {
      leader: 0,
      mission: 0,
      phase: "lobby", //start, vote, end,
      result: ""
    },
    roles: {
      merlin: false,
      percival: false,
      morgana: false,
      modred: false
    },
    voters: [],
    rules
  },
  mutations: {
    players: function(state, pkg) {
      let { type, payload } = pkg;
      if (type === "ADD") {
        state.players.push({
          name: payload.name,
          role: "",
          team: "",
          id: uniqid()
        });
      }
      if (type === "REMOVE") {
        state.players = state.players.filter(x => x.id !== payload.id);
      }
    },
    track: function(state, pkg) {
      let { type, payload } = pkg;
      if (type === "START") {
        state.track.phase = "vote";
      }
      if (type === "VOTE") {
        let currentMission = state.missions[state.track.mission];
        //Cast Vote
        if (payload.vote === "succeed") {
          currentMission.votes.succeed = currentMission.votes.succeed + 1;
        }
        if (payload.vote === "fail") {
          currentMission.votes.fail = currentMission.votes.fail + 1;
        }
        let totalVotes =
          currentMission.votes.succeed + currentMission.votes.fail;
        if (currentMission.players === totalVotes) {
          //Result
          if (currentMission.votes.fail >= currentMission.fails) {
            currentMission.status = "fail";
          } else {
            currentMission.status = "succeed";
          }
        }
      }
      if (type === "END") {
        //End of Voting
        state.track.leader =
          state.track.leader === state.players.length - 1
            ? 0
            : state.track.leader + 1;

        state.track.phase = "lobby";
        state.track.mission += 1;

        let totalFails = state.missions.filter(x => x.status === "fail").length;
        if (totalFails >= 3) {
          state.track.phase = "end";
          state.track.result = "spies";
        }

        let totalSuccesses = state.missions.filter(x => x.status === "succeed")
          .length;
        if (totalSuccesses >= 3) {
          if (state.roles.merlin) {
            state.track.phase = "guess";
          } else {
            state.track.phase = "end";
            state.track.result = "resistance";
          }
        }
      }
    },
    guess: function(state, pkg) {
      let { id } = pkg;
      let merlin = state.players.filter(x => x.id === id)[0];
      console.log(merlin);
      if (merlin.role === "merlin") {
        state.track.phase = "end";
        state.track.result = "spies";
      } else {
        state.track.phase = "end";
        state.track.result = "resistance";
      }
    },
    shuffle: function(state, pkg) {
      state.players = _.cloneDeep(pkg.players);
      let roles = [];
      let players = state.players.length;
      let rules = {
        players: state.rules.players.filter(x => x.players === players)[0],
        missions: state.rules.missions.filter(x => x.players === players)[0]
      };
      //Enter roles
      for (let i = 0; i < rules.players.resistance; i++) {
        roles.push({ role: "member", team: "resistance" });
      }
      for (let i = 0; i < rules.players.spies; i++) {
        roles.push({ role: "member", team: "spies" });
      }

      //Roles
      if (state.roles.merlin === true) {
        let index = roles.findIndex(
          x => x.team === "resistance" && x.role === "member"
        );
        roles[index].role = "merlin";
      }
      if (state.roles.percival === true) {
        let index = roles.findIndex(
          x => x.team === "resistance" && x.role === "member"
        );
        roles[index].role = "percival";
      }
      if (state.roles.morgana === true) {
        let index = roles.findIndex(
          x => x.team === "spies" && x.role === "member"
        );
        roles[index].role = "morgana";
      }
      if (state.roles.modred === true) {
        let index = roles.findIndex(
          x => x.team === "spies" && x.role === "member"
        );
        roles[index].role = "modred";
      }

      //Shuffle Roles
      roles = _.shuffle(roles);

      //Assign Roles
      state.players.forEach((x, i) => {
        x.team = roles[i].team;
        x.role = roles[i].role;
      });

      //Shuffle Players
      state.players = _.shuffle(state.players);

      //Initiate mission
      state.missions = rules.missions.missions.map(x => {
        return {
          ...x,
          status: "pending",
          votes: {
            succeed: 0,
            fail: 0
          }
        };
      });

      state.track.leader = Math.floor(Math.random() * players - 1);

      // if (pkg.restart) {
      state.track.phase = "lobby";
      state.track.mission = 0;
      // }
    },
    reset: function(state) {
      state.players = [];
      state.missions = [];
      state.track = {
        leader: 0,
        mission: 0,
        phase: "lobby", //start, vote, end,
        result: ""
      };
    },
    role: function(state, pkg) {
      let { role } = pkg;
      state.roles[role] = !state.roles[role];
      console.log(role);
    },
    change: function(state) {
      //Change Leader
      state.track.leader =
        state.track.leader === state.players.length - 1
          ? 0
          : state.track.leader + 1;
    },
    update: function(state, pkg) {
      state.players = pkg.players;
      state.missions = pkg.missions;
      state.trackc = pkg.trackc;
      state.roles = pkg.roles;
    },
    voters: function(state, pkg){
      
    }
  },
  actions: {},
  getters: {
    players: function(state) {
      return state.players;
    },
    rules: function(state) {
      return state.rules;
    },
    missions: function(state) {
      return state.missions;
    },
    track: function(state) {
      return state.track;
    },
    roles: function(state) {
      return state.roles;
    },
    state: function(state) {
      return state;
    }
  }
};
