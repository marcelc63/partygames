import uniqid from "uniqid";
import _ from "lodash";

let rules = {
  players: [],
  missions: [
    {
      location: "Airplane",
      roles: [
        "1st Class Passenger",
        "Air Marshal",
        "Mechanic",
        "Coach Passenger",
        "Flight Attendant",
        "Co-Pilot",
        "Captain"
      ]
    },
    {
      location: "Bank",
      roles: [
        "Armored Car Driver",
        "Bank Manager",
        "Loan Consultant",
        "Bank Robber",
        "Customer",
        "Security Guard",
        "Bank Teller"
      ]
    },

    {
      location: "Beach",
      roles: [
        "Beach Bartender",
        "Kite Surfer",
        "Lifeguard",
        "Thief",
        "Beach Goer",
        "Beach Photographer",
        "Ice Cream Mann"
      ]
    },
    {
      location: "Casino",
      roles: [
        "Bartender",
        "Head Security Guard",
        "Bouncer",
        "Manager",
        "Hustler",
        "Dealer",
        "Gambler"
      ]
    },
    {
      location: "Circus Tent",
      roles: [
        "Acrobat",
        "Animal Trainer",
        "Magician",
        "Visitor",
        "Fire Eater",
        "Clown",
        "Juggler"
      ]
    },
    {
      location: "Corporate Party",
      roles: [
        "Entertainer",
        "Manager",
        "Unwelcomed Guest",
        "Owner",
        "Secretary",
        "Accountant",
        "Delivery Boy"
      ]
    },
    {
      location: "Crusader Army",
      roles: ["Servant", "Bishop", "Squire", "Archer", "Knight"]
    },
    {
      location: "Day Spa",
      roles: [
        "Customer",
        "Stylist",
        "Masseuse",
        "Manicurist",
        "Makeup Artist",
        "Dermatologist",
        "Beautician"
      ]
    },
    {
      location: "Embassy",
      roles: [
        "Security Guard",
        "Secretary",
        "Ambassador",
        "Government Official",
        "Tourist",
        "Refugee",
        "Diplomat"
      ]
    },
    {
      location: "Hospital",
      roles: ["Anesthesiologist", "Intern", "Patient", "Therapist", "Surgeon"]
    },
    {
      location: "Hotel",
      roles: [
        "Doorman",
        "Seccurity Guard",
        "Manager",
        "Housekeeper",
        "Customer",
        "Bartender",
        "Bellman"
      ]
    },
    {
      location: "Military Base",
      roles: ["Medic", "Soldier", "Sniper", "Officcer", "Tank Engineer"]
    },
    {
      location: "Movie Studio",
      roles: ["Cameraman", "Director", "Costume Artist", "Actor", "Producer"]
    },
    {
      location: "Cruise",
      roles: [
        "Rich Passenger",
        "Cook",
        "Captain",
        "Bartender",
        "Musician",
        "Waiter",
        "Mechanic"
      ]
    },
    {
      location: "Train",
      roles: [
        "Mechaninc",
        "Border Patrol",
        "Security Guard",
        "Passenger",
        "Restaurant Chef",
        "Engineer",
        "Ticker Checker"
      ]
    },
    {
      location: "Pirate Ship",
      roles: [
        "Cook",
        "Sailor",
        "Slave",
        "Cannoneer",
        "Bound Prisoner",
        "Cabin Boy",
        "Brave Captain"
      ]
    },
    {
      location: "Antartica",
      roles: [
        "Medic",
        "Geologist",
        "Expedition Leader",
        "Biologist",
        "Radioman",
        "Hydrologist",
        "Meteorologist"
      ]
    },
    {
      location: "Police Station",
      roles: [
        "Detective",
        "Lawyer",
        "Journalist",
        "Criminalist",
        "Archivist",
        "Patrol Officer",
        "Criminal"
      ]
    },
    {
      location: "Restaurant",
      roles: [
        "Musician",
        "Customer",
        "Bouncer",
        "Hostess",
        "Head Chef",
        "Food Critic",
        "Waiter"
      ]
    },
    {
      location: "School",
      roles: [
        "Gym Teacher",
        "Student",
        "Principal",
        "Security Guard",
        "Janitor",
        "Lunch Lady",
        "Maintenance Man"
      ]
    },
    {
      location: "Car Service Station",
      roles: [
        "Manager",
        "Tire Specialist",
        "Biker",
        "Car Owner",
        "Car Was Operator",
        "Electrician",
        "Auto Mechanic"
      ]
    },
    {
      location: "Space Station",
      roles: [
        "Engineer",
        "Alien",
        "Space Tourist",
        "Pilot",
        "Commander",
        "Scientist",
        "Doctor"
      ]
    },
    {
      location: "Submarine",
      roles: [
        "Cook",
        "Commander",
        "Sonar Technician",
        "Elecctronics Technician",
        "Sailor",
        "Radioman",
        "Navigator"
      ]
    },
    {
      location: "Supermarket",
      roles: [
        "Customer",
        "Cashier",
        "Butcher",
        "Janitor",
        "Security Guard",
        "Food Sample Demonstrator",
        "Shelf Stocker"
      ]
    },
    {
      location: "Broadway Theater",
      roles: [
        "Director",
        "Actor",
        "Crewman",
        "Coat Check Lady",
        "Prompter",
        "Cashier",
        "Visitor"
      ]
    },
    {
      location: "University",
      roles: [
        "Graduate Student",
        "Professor",
        "Dean",
        "Psychologist",
        "Maintenance Man",
        "Student",
        "Janitor"
      ]
    }
  ],
  scenarios: [],
  questions: {
    yesno: [
      "Do you have a Boss?",
      "Is your work thrilling?",
      "Is your work place is dangerous?",
      "Are you at an old Building?",
      "You work in shifts?",
      "You are currently moving?",
      "You can see the ocean from where you are at?"
    ],
    free: [
      "Who`s your Boss?",
      "What’s the biggest thrill you have ever had at work?",
      "Do you think this place is dangerous?",
      "How did you get here today?",
      "What do you see out the window?",
      "What`s your favourite perk of the job?",
      "Is this an old building?",
      "Do they work shifts here?",
      "What’s that in your hand?",
      "What are your responsibilities?",
      "What is the most satisfying part of your job?",
      "What are the employee benefits of working here?"
    ]
  }
};

export default {
  state: {
    players: [],
    missions: [],
    track: {
      leader: 0,
      location: "",
      phase: "lobby", //start, vote, end,
      result: ""
    },
    setting: {
      timeLimit: 0,
      setTimeLimit: 300,
      spies: 1,
      mode: "yesno" //yesno, free
    },
    rules
  },
  mutations: {
    track: function(state, pkg) {
      let { type, payload } = pkg;
      if (type === "REDO") {
        state.track.phase = "lobby";
      }
      if (type === "START") {
        state.track.phase = "discuss";
      }
      if (type === "VOTE") {
        state.track.phase = "vote";
        state.timeLimit = 0;
      }
    },
    vote: function(state, pkg) {
      let { type, id } = pkg;
      if (type === "VOTE") {
        let check = state.players.filter(x => x.id === id)[0];
        console.log(check);
        if (check.team === "spies") {
          state.track.phase = "guess";
        } else {
          state.track.result = "spies";
          state.track.phase = "end";
        }
      }
    },
    guess: function(state, pkg) {
      let { location } = pkg;
      if (state.track.location === location) {
        state.track.result = "spies";
        state.track.phase = "end";
      } else {
        state.track.result = "nonspies";
        state.track.phase = "end";
      }
    },
    shuffle: function(state, pkg) {
      state.players = _.cloneDeep(pkg.players);
      let players = state.players.length;
      let mission =
        rules.missions[Math.floor(Math.random() * rules.missions.length)];

      //Roles
      let roles = _.cloneDeep(_.shuffle(mission.roles))
        .slice(0, players - state.setting.spies)
        .map(x => {
          return {
            team: "nonspies",
            role: x
          };
        });
      for (let i = 0; i < state.setting.spies; i++) {
        roles.push({ team: "spies", role: "Spy" });
      }
      roles = _.shuffle(roles);

      //Set Location
      state.track.location = mission.location;
      console.log(roles);

      //Set Time
      state.setting.timeLimit = state.setting.setTimeLimit;

      //Assign Roles
      state.players.forEach((x, i) => {
        x.team = roles[i].team;
        x.role = roles[i].role;
      });

      //Shuffle Players
      state.players = _.shuffle(state.players);
      state.track.leader = Math.floor(Math.random() * players);
      state.track.phase = "lobby";
      state.track.mission = 0;

      console.log(state);
    },
    reset: function(state) {
      state.players = [];
      state.missions = [];
      state.track = {
        leader: 0,
        mission: 0,
        phase: "lobby", //start, vote, end,
        result: ""
      };
    },
    setting: function(state, pkg) {
      let { type, payload } = pkg;
      if (type === "TIME") {
        state.setting.setTimeLimit = payload.time;
      }
      if (type === "MODE") {
        state.setting.mode = payload.mode;
      }
      if (type === "ADD TIME") {
        state.setting.timeLimit += payload.time;
      }
      if (type === "ADD SPIES") {
        if (state.setting.spies < payload.players) {
          state.setting.spies += 1;
        }
      }
      if (type === "REDUCE SPIES") {
        if (state.setting.spies !== 1) {
          state.setting.spies -= 1;
        }
      }
    }
  },
  actions: {},
  getters: {
    players: function(state) {
      return state.players;
    },
    rules: function(state) {
      return state.rules;
    },
    missions: function(state) {
      return state.missions;
    },
    location: function(state) {
      return state.location;
    },
    locations: function(state) {
      return state.rules.missions.map(x => x.location);
    },
    track: function(state) {
      return state.track;
    },
    roles: function(state) {
      return state.roles;
    },
    timeLimit: function(state) {
      return state.setting.timeLimit;
    },
    setting: function(state) {
      return state.setting;
    }
  }
};
