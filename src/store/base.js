import uniqid from "uniqid";
import _ from "lodash";

let test = [
  {
    name: "c",
    id: "c",
    status: "offline",
    avatar: "https://i.imgur.com/REqgXOt.png"
  },
  {
    name: "d",
    id: "d",
    status: "offline",
    avatar: "https://i.imgur.com/REqgXOt.png"
  },
  {
    name: "e",
    id: "e",
    status: "offline",
    avatar: "https://i.imgur.com/REqgXOt.png"
  }
];

export default {
  state: {
    profile: {
      name: "",
      id: "",
      status: "", //Host or Participant,
      avatar: ""
    },
    players: [...test],
    room: "",
    game: "none", //To know where to route data to which store
    route: "room", //Programmatic Routing - so can be easily controlled via socket
    chats: [],
    newChats: 0
  },
  mutations: {
    name: function(state, pkg) {
      state.profile.name = pkg.name;
    },
    avatar: function(state, pkg) {
      state.profile.avatar = pkg.avatar;
    },
    setID: function(state, pkg) {
      if (state.profile.id === "") {
        state.profile.id = uniqid();
      }
    },
    initiate: function(state, pkg) {
      state.room = pkg.room;
      console.log(state.room);
      state.profile.status = pkg.status;
      let check = state.players.findIndex(x => x.id === state.profile.id);
      if (check === -1) {
        state.players.push({ ...state.profile });
      }
    },
    addPlayer: function(state, pkg) {
      if (pkg.type === "offline") {
        state.players.push({
          name: pkg.name,
          id: uniqid(),
          status: "offline",
          avatar: pkg.avatar
        });
      }
      if (pkg.type === "online") {
        state.players.push({
          name: pkg.name,
          id: pkg.id,
          status: "participant",
          avatar: pkg.avatar
        });
      }
    },
    removePlayer: function(state, pkg) {
      state.players = state.players.filter(x => x.id !== pkg.id);
    },
    room: function(state, pkg) {},
    exit: function(state, pkg) {},
    route: function(state, pkg) {
      state.route = pkg.route;
    },
    game: function(state, pkg) {
      state.game = pkg.game;
    },
    sync: function(state, pkg) {
      if (pkg.type === "players") {
        state.players = pkg.data;
      }
      if (pkg.type === "game") {
        state.game = pkg.data;
        state.route = "mode";
      }
    },
    chat: function(state, pkg) {
      state.chats.push(pkg.chat);
      if (pkg.chat.id !== state.profile.id) {
        state.newChats = state.newChats + 1;
      }
    },
    readChat: function(state, pkg) {
      state.newChats = 0;
    }
  },
  actions: {},
  getters: {
    players: function(state) {
      return state.players;
    },
    profile: function(state) {
      return state.profile;
    },
    room: function(state) {
      return state.room;
    },
    route: function(state) {
      return state.route;
    },
    game: function(state) {
      return state.game;
    },
    chats: function(state) {
      return state.chats;
    },
    newChats: function(state) {
      return state.newChats;
    }
  }
};
