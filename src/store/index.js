import Vue from "vue";
import Vuex from "vuex";
import uniqid from "uniqid";
import _ from "lodash";
import base from "./base.js";
import avalon from "./avalon.js";
import undercover from "./undercover.js";
import werewolf from "./werewolf.js";

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    base: { namespaced: true, ...base },
    avalon: { namespaced: true, ...avalon },
    undercover: { namespaced: true, ...undercover },
    werewolf: { namespaced: true, ...werewolf }
  }
});
