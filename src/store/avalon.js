import uniqid from "uniqid";
import _ from "lodash";

let rules = {
  players: [
    { players: 5, resistance: 3, spies: 2 },
    { players: 6, resistance: 4, spies: 2 },
    { players: 7, resistance: 4, spies: 3 },
    { players: 8, resistance: 5, spies: 3 },
    { players: 9, resistance: 6, spies: 3 },
    { players: 10, resistance: 6, spies: 4 },
    { players: 11, resistance: 7, spies: 4 },
    { players: 12, resistance: 8, spies: 4 }
  ],
  missions: [
    {
      players: 5,
      missions: [
        { players: 2, fails: 1 },
        { players: 3, fails: 1 },
        { players: 2, fails: 1 },
        { players: 3, fails: 1 },
        { players: 3, fails: 1 }
      ]
    },
    {
      players: 6,
      missions: [
        { players: 2, fails: 1 },
        { players: 3, fails: 1 },
        { players: 4, fails: 1 },
        { players: 3, fails: 1 },
        { players: 4, fails: 1 }
      ]
    },
    {
      players: 7,
      missions: [
        { players: 2, fails: 1 },
        { players: 3, fails: 1 },
        { players: 3, fails: 1 },
        { players: 4, fails: 2 },
        { players: 4, fails: 1 }
      ]
    },
    {
      players: 8,
      missions: [
        { players: 3, fails: 1 },
        { players: 4, fails: 1 },
        { players: 4, fails: 1 },
        { players: 5, fails: 2 },
        { players: 5, fails: 1 }
      ]
    },
    {
      players: 9,
      missions: [
        { players: 3, fails: 1 },
        { players: 4, fails: 1 },
        { players: 4, fails: 1 },
        { players: 5, fails: 2 },
        { players: 5, fails: 1 }
      ]
    },
    {
      players: 10,
      missions: [
        { players: 3, fails: 1 },
        { players: 4, fails: 1 },
        { players: 4, fails: 1 },
        { players: 5, fails: 2 },
        { players: 5, fails: 1 }
      ]
    },
    {
      players: 11,
      missions: [
        { players: 3, fails: 1 },
        { players: 4, fails: 1 },
        { players: 4, fails: 1 },
        { players: 5, fails: 2 },
        { players: 5, fails: 1 }
      ]
    },
    {
      players: 12,
      missions: [
        { players: 3, fails: 1 },
        { players: 4, fails: 1 },
        { players: 4, fails: 1 },
        { players: 5, fails: 2 },
        { players: 5, fails: 1 }
      ]
    }
  ]
};

export default {
  state: {
    room: "",
    players: [],
    missions: [],
    phase: "mode",
    track: {
      leader: 0,
      mission: 0,
      result: ""
    },
    voters: [],
    votes: {
      succeed: 0,
      fail: 0,
      total: 0
    },
    elected: "",
    roles: {
      merlin: false,
      percival: false,
      morgana: false,
      modred: false
    },
    rules
  },
  mutations: {
    initiate: function(state, pkg) {
      state.players = pkg.players.map(x => {
        return {
          ...x,
          role: "",
          team: ""
        };
      });
      state.room = pkg.room;
    },
    role: function(state, pkg) {
      state.roles[pkg.role] = !state.roles[pkg.role];
    },
    shuffle: function(state, pkg) {
      let players = state.players.length;
      let roles = [];
      let rules = {
        players: state.rules.players.filter(x => x.players === players)[0],
        missions: state.rules.missions.filter(x => x.players === players)[0]
      };
      //Enter roles
      for (let i = 0; i < rules.players.resistance; i++) {
        roles.push({ role: "player", team: "resistance" });
      }
      for (let i = 0; i < rules.players.spies; i++) {
        roles.push({ role: "player", team: "spies" });
      }

      //Roles
      if (state.roles.merlin === true) {
        let index = roles.findIndex(
          x => x.team === "resistance" && x.role === "player"
        );
        roles[index].role = "merlin";
      }
      if (state.roles.percival === true) {
        let index = roles.findIndex(
          x => x.team === "resistance" && x.role === "player"
        );
        roles[index].role = "percival";
      }
      if (state.roles.morgana === true) {
        let index = roles.findIndex(
          x => x.team === "spies" && x.role === "player"
        );
        roles[index].role = "morgana";
      }
      if (state.roles.modred === true) {
        let index = roles.findIndex(
          x => x.team === "spies" && x.role === "player"
        );
        roles[index].role = "modred";
      }

      //Shuffle Roles
      roles = _.shuffle(roles);

      //Assign Roles
      state.players.forEach((x, i) => {
        x.team = roles[i].team;
        x.role = roles[i].role;
      });

      //Shuffle Players
      state.players = _.shuffle(state.players);

      //Initiate mission
      state.missions = rules.missions.missions.map(x => {
        return {
          ...x,
          status: "pending",
          votes: {
            succeed: 0,
            fail: 0
          }
        };
      });

      state.track.leader = Math.floor(Math.random() * players);
      state.track.mission = 0;
    },
    voters: function(state, pkg) {
      let check =
        state.voters.length < state.missions[state.track.mission].players;
      if (!state.voters.some(x => x.id === pkg.player.id) && check) {
        state.voters.push(pkg.player);
      } else {
        state.voters = state.voters.filter(x => x.id !== pkg.player.id);
      }
    },
    vote: function(state, pkg) {
      if (pkg.type === "VOTE") {
        if (pkg.vote === "succeed") {
          state.votes.succeed = state.votes.succeed + 1;
        }
        if (pkg.vote === "fail") {
          state.votes.fail = state.votes.fail + 1;
        }
        state.votes.total = state.votes.total + 1;
      }
      if (pkg.type === "CALCULATE") {
        let currentMission = state.missions[state.track.mission];
        //Cast Vote
        currentMission.votes.succeed = state.votes.succeed;
        currentMission.votes.fail = state.votes.fail;
        let totalVotes =
          currentMission.votes.succeed + currentMission.votes.fail;
        if (currentMission.players === totalVotes) {
          //Result
          if (currentMission.votes.fail >= currentMission.fails) {
            currentMission.status = "fail";
          } else {
            currentMission.status = "succeed";
          }
        }
        state.track.leader = state.track.leader + 1;
        if (state.track.leader > state.players.length - 1) {
          state.track.leader = 0;
        }
        console.log(currentMission);
      }
    },
    nextMission: function(state, pkg) {
      state.track.mission = state.track.mission + 1;
      state.votes = {
        succeed: 0,
        fail: 0,
        total: 0
      };
      state.voters = [];
    },
    endGame: function(state, pkg) {
      let succeed = state.missions.filter(x => x.status === "succeed").length;
      let fail = state.missions.filter(x => x.status === "fail").length;
      if (succeed === 3) {
        state.track.result = "resistance";
      }
      if (fail === 3) {
        state.track.result = "spies";
      }
    },
    guess: function(state, pkg) {
      if (pkg.type === "ELECT") {
        state.elected = pkg.id;
      }
      if (pkg.type === "GUESS") {
        let merlin = state.players.filter(x => x.id === state.elected)[0];
        if (merlin.role === "merlin") {
          state.track.result = "spies";
        }
        console.log(state.track.result, "GUESS");
      }
    },
    reset: function(state, pkg) {
      state.track = {
        leader: 0,
        mission: 0,
        result: ""
      };
      state.votes = {
        succeed: 0,
        fail: 0,
        total: 0
      };
      state.missions = [];
      state.voters = [];
      state.elected = "";
    },
    sync: function(state, pkg) {
      if (pkg.type === "players") {
        state.players = pkg.data;
      } else if (pkg.type === "roles") {
        state.roles = pkg.data;
      } else {
        state[pkg.key] = pkg.data;
      }
    },
    feedback: function(state, pkg) {
      if (pkg.type === "vote") {
        if (pkg.data === "succeed") {
          state.votes.succeed = state.votes.succeed + 1;
        }
        if (pkg.data === "fail") {
          state.votes.fail = state.votes.fail + 1;
        }
        state.votes.total = state.votes.total + 1;
      }
    },
    changeLeader: function(state, pkg) {
      state.track.leader = state.track.leader + 1;
      if (state.track.leader > state.players.length - 1) {
        state.track.leader = 0;
      }
      state.voters = [];
    }
  },
  actions: {},
  getters: {
    players: function(state) {
      return state.players;
    },
    rules: function(state) {
      let players = state.players.length;
      let rule = {
        players: state.rules.players.filter(x => x.players === players)[0],
        missions: state.rules.missions.filter(x => x.players === players)[0]
      };
      return rule;
    },
    missions: function(state) {
      return state.missions;
    },
    voters: function(state) {
      return state.voters;
    },
    track: function(state) {
      return state.track;
    },
    roles: function(state) {
      return state.roles;
    },
    votes: function(state) {
      return state.votes;
    },
    elected: function(state) {
      return state.elected;
    },
    state: function(state) {
      return state;
    }
  }
};
