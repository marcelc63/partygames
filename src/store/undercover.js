import uniqid from "uniqid";
import _ from "lodash";
import x from "uniqid";

let rules = {
  wordBank: [
    {
      category: "Coffee",
      words: ["Kopi Kenangan", "Janji Jiwa", "Kulo", "Tuku"]
    },
    {
      category: "Water",
      words: ["Aqua", "Ades", "Le Menirale", "Nestle PureLife"]
    },
    {
      category: "Bioskop",
      words: ["XXI", "CGV", "Cinepolis", "Flix"]
    },
    {
      category: "Boba",
      words: ["Kokumi", "Xing Fu Tang", "Tiger Sugar", "XiBoBa"]
    },
    // {
    //   category: "Soft Drinks",
    //   words: ["Coca Cola", "Pepsi", "Sprite", "Fanta"]
    // },
    // {
    //   category: "Fast Food",
    //   words: ["McDonald", "Burger King", "KFC", "A&W"]
    // },
    {
      category: "Pets",
      words: ["Cats", "Dogs", "Rabbit", "Hamster"]
    },
    // {
    //   category: "Vehicle",
    //   words: ["Car", "Minivan", "Bus", "Truck"]
    // },
    // {
    //   category: "Flying Vehicles",
    //   words: ["Airplane", "Helicopter", "Rocket", "Paraglider"]
    // },
    {
      category: "Red Fruits",
      words: ["Strawberry", "Cherry", "Rambutan", "Lychee"]
    },
    {
      category: "Orange Fruits",
      words: ["Mango", "Banana", "Pineapple", "Starfruit"]
    },
    {
      category: "Avengers",
      words: ["Ironman", "Captain America", "Hulk", "Thor"]
    },
    // {
    //   category: "Chris' Celebs",
    //   words: ["Chris Evan", "Chris Hemsworth", "Chris Pratt", "Chris Pine"]
    // },
    {
      category: "Social Media",
      words: ["Facebook", "Instagram", "YouTube", "TikTok"]
    },
    // {
    //   category: "KPOP Boy band",
    //   words: ["Big Bang", "Super Junior", "2PM", "BTS"]
    // },
    // {
    //   category: "Sandwhiches",
    //   words: ["Burger", "Hotdog", "Sandwhich", "Panini"]
    // },
    // {
    //   category: "Birds",
    //   words: ["Chicken", "Duck", "Turkey", "Goose"]
    // },
    // {
    //   category: "Utensils",
    //   words: ["Spoon", "Fork", "Knife", "Chopsticks"]
    // },
    // {
    //   category: "Cars",
    //   words: ["Toyota", "Honda", "Suzuki", "Mitsubishi"]
    // },
    // {
    //   category: "Luxury Cars",
    //   words: ["BMW", "Mercedes", "VW", "Audi"]
    // }
  ]
};

export default {
  state: {
    players: [],
    rules: rules,
    words: {
      player: "",
      spies: "",
      all: []
    },
    elected: "",
    guessWord: "",
    track: {
      leader: 0,
      result: ""
    },
    roles: {
      spies: 1,
      mrwhite: 0
    }
  },
  mutations: {
    initiate: function(state, pkg) {
      state.players = pkg.players.map(x => {
        return {
          ...x,
          role: "",
          eliminated: false
        };
      });
      state.room = pkg.room;
    },
    reset: function(state, pkg) {
      state.players = state.players.map(x => {
        return {
          ...x,
          eliminated: false
        };
      });
      state.track.result = "";
      state.elected = "";
    },
    shuffle: function(state, pkg) {
      let totalPlayers = state.players.length;
      let players = totalPlayers - state.roles.spies - state.roles.mrwhite;
      let roles = [];

      //Enter roles
      for (let i = 0; i < players; i++) {
        roles.push({
          role: "player"
        });
      }
      for (let i = 0; i < state.roles.spies; i++) {
        roles.push({
          role: "spies"
        });
      }
      for (let i = 0; i < state.roles.mrwhite; i++) {
        roles.push({
          role: "mrwhite"
        });
      }

      //Shuffle Roles
      roles = _.shuffle(roles);

      //Assign Roles
      state.players.forEach((x, i) => {
        x.role = roles[i].role;
      });

      //Shuffle Players
      state.players = _.shuffle(state.players);

      //Set Words
      let wordBank = _.shuffle(state.rules.wordBank)[0];
      let words = _.shuffle(wordBank.words);
      console.log(state.rules, wordBank, words);
      state.words.player = words[0];
      state.words.spies = words[1];
      state.words.all = _.shuffle(words);

      state.track.leader = Math.floor(Math.random() * totalPlayers);
    },
    role: function(state, pkg) {
      if (pkg.type === "add") {
        state.roles[pkg.role] = state.roles[pkg.role] + 1;
      }
      if (pkg.type === "subtract") {
        state.roles[pkg.role] = state.roles[pkg.role] - 1;
      }
    },
    elect: function(state, pkg) {
      state.elected = pkg.id;
    },
    eliminate: function(state, pkg) {
      let player = state.players.filter(x => x.id === state.elected)[0];
      player.eliminated = true;

      let spies = state.players.filter(
        x => x.role === "spies" && x.eliminated === false
      ).length;
      let mrwhite = state.players.filter(
        x => x.role === "mrwhite" && x.eliminated === false
      ).length;
      let players = state.players.filter(
        x => x.role === "player" && x.eliminated === false
      ).length;
      if (players === 1) {
        state.track.result = "spies";
      }
      if (spies === 0 && mrwhite === 0) {
        state.track.result = "players";
      }
    },
    guess: function(state, pkg) {
      if (pkg.type === "CHOOSE") {
        state.guessWord = pkg.word;
      }
      if (pkg.type === "GUESS") {
        if (state.guessWord === state.words.player) {
          state.track.result = "spies";
        }
      }
    }
  },
  actions: {},
  getters: {
    players: function(state) {
      return state.players;
    },
    roles: function(state) {
      return state.roles;
    },
    rules: function(state) {
      return state.rules;
    },
    track: function(state) {
      return state.track;
    },
    words: function(state) {
      return state.words;
    },
    elected: function(state) {
      return state.elected;
    },
    guessWord: function(state) {
      return state.guessWord;
    }
  }
};
