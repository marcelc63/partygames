import uniqid from "uniqid";
import _ from "lodash";
import x from "uniqid";

let rules = {};

export default {
  state: {
    players: [],
    rules: rules,
    elected: "",    
    track: {
      result: "",
      killed: "",
      captured: ""
    },
    roles: {
      werewolf: 1,
      doctor: 1,
      seer: 1
    }
  },
  mutations: {
    initiate: function(state, pkg) {
      console.log("initate");
      state.players = pkg.players.map(x => {
        return {
          ...x,
          role: "",
          eliminated: false,
          kill: 0,
          protect: false
        };
      });
      state.room = pkg.room;
    },
    reset: function(state, pkg) {
      state.players = state.players.map(x => {
        return {
          ...x,
          role: "",
          eliminated: false,
          kill: 0,
          protect: false
        };
      });
      state.track.result = "";
      state.track.killed = "";
      state.track.captured = "";
    },
    shuffle: function(state, pkg) {
      let totalPlayers = state.players.length;
      let players =
        totalPlayers -
        state.roles.werewolf -
        state.roles.doctor -
        state.roles.seer;
      let roles = [];

      //Enter roles
      for (let i = 0; i < players; i++) {
        roles.push({
          role: "villager"
        });
      }
      for (let i = 0; i < state.roles.werewolf; i++) {
        roles.push({
          role: "werewolf"
        });
      }
      for (let i = 0; i < state.roles.doctor; i++) {
        roles.push({
          role: "doctor"
        });
      }
      for (let i = 0; i < state.roles.seer; i++) {
        roles.push({
          role: "seer"
        });
      }

      //Shuffle Roles
      roles = _.shuffle(roles);

      //Assign Roles
      state.players.forEach((x, i) => {
        x.role = roles[i].role;
      });

      //Shuffle Players
      state.players = _.shuffle(state.players);
    },
    role: function(state, pkg) {
      if (pkg.type === "add") {
        state.roles[pkg.role] = state.roles[pkg.role] + 1;
      }
      if (pkg.type === "subtract") {
        state.roles[pkg.role] = state.roles[pkg.role] - 1;
      }
    },
    elect: function(state, pkg) {
      state.elected = pkg.id;
    },
    eliminate: function(state, pkg) {
      let player = state.players.filter(x => x.id === state.elected)[0];
      player.eliminated = true;

      let spies = state.players.filter(
        x => x.role === "spies" && x.eliminated === false
      ).length;
      let mrwhite = state.players.filter(
        x => x.role === "mrwhite" && x.eliminated === false
      ).length;
      let players = state.players.filter(
        x => x.role === "player" && x.eliminated === false
      ).length;
      if (players === 1) {
        state.track.result = "spies";
      }
      if (spies === 0 && mrwhite === 0) {
        state.track.result = "players";
      }
    },
    guess: function(state, pkg) {
      if (pkg.type === "CHOOSE") {
        state.track.captured = pkg.id;
      }
      if (pkg.type === "GUESS") {
        let playerIn = state.players.findIndex(
          x => x.id === state.track.captured
        );
        let player = state.players[playerIn];
        player.eliminated = true;

        //Check Winning Condition
        let villagers = state.players.filter(
          x => x.role !== "werewolf" && x.eliminated === false
        ).length;
        let enemies = state.players.filter(
          x => x.role === "werewolf" && x.eliminated === false
        ).length;

        //End Game
        if (villagers === enemies) {
          state.track.result = "werewolf";
        }
        if (enemies === 0) {
          state.track.result = "villagers";
        }
      }
    },
    nextPhase: function(state, pkg) {
      state.track.killed = "";
      state.track.captured = "";
    },
    calculate: function(state, pkg) {
      //Check Killed
      let killed = state.players.filter(x => x.kill > 0);
      killed = killed.sort((a, b) => b.kill - a.kill);
      console.log(killed);
      //Check Draw

      //Kill
      let playerIn = state.players.findIndex(x => x.id === killed[0].id);
      let player = state.players[playerIn];
      if (!player.protect) {
        state.players[playerIn].eliminated = true;
        state.track.killed = player.id;
      }

      //Check Winning Condition
      let villagers = state.players.filter(
        x => x.role !== "werewolf" && x.eliminated === false
      ).length;
      let enemies = state.players.filter(
        x => x.role === "werewolf" && x.eliminated === false
      ).length;

      //End Game
      if (villagers === enemies) {
        state.track.result = "werewolf";
      }
      if (enemies === 0) {
        state.track.result = "villagers";
      }
    },
    action: function(state, pkg) {
      console.log("action", pkg);
      if (pkg.type === "KILL") {
        let playerIn = state.players.findIndex(x => x.id === pkg.id);
        state.players[playerIn].kill = state.players[playerIn].kill + 1;
        console.log("killed", state.players[playerIn]);
      }
      if (pkg.type === "PROTECT") {
        let playerIn = state.players.findIndex(x => x.id === pkg.id);
        state.players[playerIn].protect = true;
      }
    }
  },
  actions: {},
  getters: {
    players: function(state) {
      return state.players;
    },
    roles: function(state) {
      return state.roles;
    },
    rules: function(state) {
      return state.rules;
    },
    track: function(state) {
      return state.track;
    },
    words: function(state) {
      return state.words;
    },
    elected: function(state) {
      return state.elected;
    },
    guessWord: function(state) {
      return state.guessWord;
    }
  }
};
