//Dependencies
const express = require("express");
const app = express();
let http = require("http").Server(app);
let port = 3011;
var path = require("path");

require("./sockets/index.js")(http);

app.use(express.static(__dirname + "/../dist"));

app.get("*", function(req, res) {
  res.sendFile(path.join(__dirname + "/../dist/index.html"));
});

//Initiate
http.listen(port, function() {
  console.log("listening on *:", port);
});
