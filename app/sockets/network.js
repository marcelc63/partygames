let uniqid = require("uniqid");

//Things to have
/*
Active Room
- Host
- Participants

Transmission Functions
- Host to Participants
-- Joining Room
-- Game Actions
- Participants to Host
-- Broadcast
*/
let rooms = [];

module.exports = function(io, socket) {
  //Room Functions
  socket.on("create", packet => {
    // let id = uniqid.time();
    let id = packet.id;
    console.log("hii", id);
    rooms.push({
      id: id,
      host: socket.id
    });
    socket.join(id);
    // socket.emit("joined", id);
  });
  socket.on("leave", packet => {
    if (packet.type === "host") {
      let id = packet.id;
      let check = rooms.findIndex(x => x.id === id);
      if (check !== -1) {
        let room = rooms[check];
        socket.to(packet.id).emit("leave");
        rooms = rooms.filter(x => x.id !== id);
      }
      socket.leave(packet.id);
    }
    if (packet.type === "remove") {
      let id = packet.id;
      let check = rooms.findIndex(x => x.id === id);
      if (check !== -1) {
        socket.to(packet.player).emit("leave");
      }
    }
    if (packet.type === "exit") {
      let id = packet.id;
      let check = rooms.findIndex(x => x.id === id);
      if (check !== -1) {
        let room = rooms[check];
        socket.to(room.host).emit("playerLeave");
      }
      socket.leave(packet.id);
    }
    if (packet.type === "leave") {
      socket.leave(packet.id);
    }
  });

  //Host Transmission
  socket.on("broadcast", packet => {
    console.log("broadcast", packet);
    socket.to(packet.id).emit("receiver", {
      ...packet
    });
  });

  //Participant Transmission
  socket.on("join", packet => {
    let id = packet.id;
    let check = rooms.findIndex(x => x.id === id);
    console.log(check, rooms);
    if (check !== -1) {
      let room = rooms[check];
      console.log("awesome!", room);
      socket.join(room.id);

      socket.emit("joined", room.id);
      io.to(room.host).emit("host", {
        type: "players",
        profile: packet.profile
      });
    }
  });
  socket.on("feedback", packet => {
    let id = packet.id;
    let check = rooms.findIndex(x => x.id === id);
    if (check !== -1) {
      let room = rooms[check];
      io.to(room.host).emit("host", {
        type: "feedback",
        ...packet
      });
    }
  });

  //Chat
  socket.on("chat", packet => {
    let id = packet.id;
    let check = rooms.findIndex(x => x.id === id);
    if (check !== -1) {
      console.log("chat");
      let room = rooms[check];
      io.to(room.id).emit("chat", {
        ...packet
      });
    }
  });
};
