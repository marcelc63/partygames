let games = [];

function getGame(room) {
  let index = games.findIndex(x => x.room === room);
  return games[index];
}

function roomCheck(room) {
  //Check
  let check = games.some(x => x.room === room);
  if (!check) {
    return false;
  }
  return true;
}

module.exports = function(io, socket) {
  socket.on("create", packet => {
    console.log("create game", packet.room);
    games.push({
      room: packet.room,
      games: "",
      master: socket.id,
      players: [packet.player]
    });
    socket.join(packet.room);
  });

  socket.on("join", packet => {
    //Check
    if (!roomCheck(packet.room)) {
      return false;
    }

    socket.join(packet.room);

    let game = getGame(packet.room);
    game.players.push(packet.player);

    io.to(packet.room).emit("joined", game.players);
    io.to(game.room).emit("players", game.players);
  });

  //Updates
  socket.on("players", packet => {
    console.log("add player", packet.room);
    //Check
    if (!roomCheck(packet.room)) {
      return false;
    }
    let game = getGame(packet.room);
    game.players = packet.players;
    io.to(game.room).emit("players", game.players);
  });

  socket.on("game", packet => {
    console.log("choose game", packet.game);
    //Check
    if (!roomCheck(packet.room)) {
      return false;
    }
    let game = getGame(packet.room);
    game.games = packet.game;
    console.log(game);
    io.to(game.room).emit("startGame", game.games);
  });

  socket.on("update", packet => {
    console.log("choose game", packet.game);
    //Check
    if (!roomCheck(packet.room)) {
      return false;
    }
    let game = getGame(packet.room);
    console.log(game);
    io.to(game.room).emit("updateGame", {
      game: game.games,
      data: packet.data
    });
  });

  socket.on("route", packet => {
    console.log("choose game", packet.game);
    //Check
    if (!roomCheck(packet.room)) {
      return false;
    }
    let game = getGame(packet.room);
    console.log(game);
    io.to(game.room).emit("routeGame", {
      game: game.games,
      route: packet.route
    });
  });
};
