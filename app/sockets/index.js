module.exports = function(http) {
  let io = require("socket.io")(http);
  //Connection
  io.on("connection", function(socket) {
    console.log("connected");
    // require("./game.js")(io, socket);
    require("./network.js")(io, socket);
  });
};
